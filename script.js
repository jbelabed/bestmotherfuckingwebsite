document.querySelector("#contrast").addEventListener('click',()=>{
    if (document.querySelector("#contrast").innerHTML==="Add more contrast") {
        document.querySelector("#contrast").innerHTML="Remove additional contrast";
        if (document.querySelector('#invmode').innerHTML==="Inverted mode") {
        document.querySelector('body').style.color="black";
        } else {
            document.querySelector('body').style.color="#fff";
        }
    } else {
        document.querySelector("#contrast").innerHTML="Add more contrast";
        if (document.querySelector('#invmode').innerHTML==="Inverted mode") {
            document.querySelector('body').style.color="#454545";
        }else {
            document.querySelector('body').style.color="#bababa";
        }
    }
})


document.querySelector('#invmode').addEventListener('click', ()=>{
    if (document.querySelector('#invmode').innerHTML==="Inverted mode") {
        document.querySelector("#contrast").style.color="#fff";
        document.querySelector('#invmode').innerHTML="Normal mode";
        document.querySelector('html').style.backgroundColor="black";
        if (document.querySelector("#contrast").innerHTML==="Add more contrast"){
            document.querySelector('body').style.color="#bababa";
        } else {
            document.querySelector('body').style.color="#fff";
        }
    } else {
        document.querySelector('html').style.backgroundColor="#fff";
        document.querySelector('#invmode').innerHTML="Inverted mode";
        document.querySelector("#contrast").style.color="#000";
        if (document.querySelector("#contrast").innerHTML==="Add more contrast") {
            document.querySelector('body').style.color="#454545";
        } else {
            document.querySelector('body').style.color="#050505"
        }
    }
})